import 'package:flutter/material.dart';
import '../services/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './newcustomer.dart';
import './customer.dart';

// List of all the added customers
class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.onSignedOut})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback onSignedOut;
  final String userId;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    final topAppBar = AppBar(
      elevation: 0.1,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      title: Text('Customers'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.exit_to_app),
          tooltip: 'Logout',
          onPressed: () {
            _signOut();
            print('logged out command should happen here');
          },
        ),
      ],
    );

    Widget makeListTile(dynamic document) {
      print(document);
      return ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.white24))),
            child: Icon(Icons.account_circle, color: Colors.white),
          ),
          title: Text(
            document['name'],
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          subtitle: Row(
            children: <Widget>[
              Text(document['phone'], style: TextStyle(color: Colors.white))
            ],
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        CustomerDetailPage(customer: document)));
          },
          trailing: Icon(Icons.keyboard_arrow_right,
              color: Colors.white, size: 30.0));
    }

    Widget makeCard(dynamic documents) {
      return Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: makeListTile(documents),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: topAppBar,
      // Using Firestore as the backend here
      // User submitted customers are saved in the collection endpoint below
      // /customer/user_id/customers
      body: StreamBuilder(
          stream: Firestore.instance
              .collection('customer/${widget.userId}/customers').orderBy("createdAt", descending: true)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              print(snapshot.error);
              return Text('Error: ${snapshot.error}');
            }

            if (snapshot.hasData && snapshot.data.documents.length < 1) {
              return Center(
                  child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    'No Customer Data',
                    style: TextStyle(fontSize: 32.0, color: Colors.white),
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    'Use button below to add customer',
                    style: TextStyle(fontSize: 14.0, color: Colors.white70),
                  )
                ],
              ));
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                );
              default:
                print(snapshot.data.documents);
                return ListView(
                  children: snapshot.data.documents.map(
                    (DocumentSnapshot document) {
                      return makeCard(document);
                    },
                  ).toList(),
                );
            }
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => NewCustomerPage()));
        },
        tooltip: 'Add Report',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
