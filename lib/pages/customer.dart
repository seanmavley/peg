import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

// Displays the detail info of a customer

class CustomerDetailPage extends StatefulWidget {
  final customer;
  CustomerDetailPage({Key key, this.customer}) : super(key: key);
  _CustomerDetailPageState createState() => _CustomerDetailPageState();
}

class _CustomerDetailPageState extends State<CustomerDetailPage> {
  var _address;
  double lat;
  double lon;

  _launchURL(double lat, double lon) async {
    var url = 'http://maps.google.com/maps?q=loc:$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _locationAddress() async {
    var address;
    double _lat =
        double.parse(widget.customer['location'].toString().split(',')[0]);
    double _lon =
        double.parse(widget.customer['location'].toString().split(',')[1]);
    try {
      address = await Geolocator().placemarkFromCoordinates(lat, lon);
    } on PlatformException {
      address = 'Could not find address. Click link below';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    setState(() {
      // we update a few vars
      _address = address;
      lat = _lat;
      lon = _lon;
    });
  }

  @override
  void initState() {
    super.initState();
    _locationAddress();
  }

  @override
  Widget build(BuildContext context) {

    final locationIndicator = Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        'Location: ${widget.customer['location']}',
        style: TextStyle(color: Colors.white),
      ),
    );

    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 18.0),
        Text(
          widget.customer['name'],
          style: TextStyle(color: Colors.white, fontSize: 24.0),
        ),
        SizedBox(
          height: 24.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[locationIndicator],
        ),
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(40.0),
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: topContentText,
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );

    final readButton = Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          onPressed: () {
               _launchURL(lat, lon);
              },
          color: Color.fromRGBO(58, 66, 86, 1.0),
          child: Text("View location", style: TextStyle(color: Colors.white)),
        ));

    final bottomContent = Container(
      padding: EdgeInsets.all(40.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            'Address: $_address',
            style: TextStyle(fontSize: 18.0),
          ),
          SizedBox(
            height: 12.0,
          ),
          readButton,
        ],
      ),
    );

    return Scaffold(
      body: Column(
        children: <Widget>[topContent, bottomContent],
      ),
    );
  }
}
