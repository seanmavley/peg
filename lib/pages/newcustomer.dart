import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/services.dart';
import '../services/customer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../services/auth.dart';

// Add a new customer
class NewCustomerPage extends StatefulWidget {
  _NewCustomerPageState createState() => _NewCustomerPageState();
}

class _NewCustomerPageState extends State<NewCustomerPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  Position _position;
  bool _busy = false;
  Auth auth = new Auth();
  CustomerService toSave = new CustomerService();

  // To grab the geolocation of the user seamlessly
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initGeoLocationRequest() async {
    Position position;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      final Geolocator geolocator = Geolocator()
        ..forceAndroidLocationManager = true;
      position = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.bestForNavigation);
    } on PlatformException {
      position = null;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    setState(() {
      // this updated reflects in the locationController.text field
      // below
      _position = position;
    });
  }

  // 18 years ago
  DateTime _date = new DateTime.now().subtract(new Duration(days: 6570));

  // Popup to select date, tied to a formfield
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      initialDatePickerMode: DatePickerMode.year,
      context: context,
      initialDate: _date, // allow date as far back as 18 years, from *today*
      firstDate: new DateTime(1950),
      lastDate: _date,
    );
    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
      });
      dobController.text = _date.toString().split(' ')[0];
    }
  }

  // Save form, and send details to endpoint
  // first to firestore (firebase), then to PEG endpoint
  void submit() async {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();

      auth.getCurrentUser().then((res) {
        // save to firebase
        Firestore.instance
            .collection('customer')
            .document('${res.uid}')
            .collection('customers')
            .add({
          "name": customerNameController.text,
          "location": locationController.text,
          "dob": dobController.text,
          "phone": phoneController.text,
          "createdAt": FieldValue.serverTimestamp()
        }).then((res) {
          // save to peg endpoint
          toSave.saveToPeg({
            "customer_name": customerNameController.text,
            "location": locationController.text,
            "dob": dobController.text,
            "phone_number": phoneController.text,
          }).then((resPeg) {
            print(resPeg);
            print(res.get());
            setState(() {
              _busy = false;
            });
            Navigator.pop(context);
          });
        });
      });
    }
  }

  // Field is required validation
  String _isRequired(String value) {
    if (value.length < 1) {
      scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Field is Required'),
      ));
      return 'Field is required';
    }
    return null;
  }

  final TextEditingController dobController =
      new TextEditingController(text: '');

  final TextEditingController customerNameController =
      new TextEditingController(text: '');

  final TextEditingController phoneController =
      new TextEditingController(text: '');

  final TextEditingController locationController =
      new TextEditingController(text: '');

  @override
  void initState() {
    super.initState();
    _initGeoLocationRequest();
  }

  @override
  Widget build(BuildContext context) {
    // update location text field
    if (_position != null) {
      print('${_position?.latitude} ${_position?.longitude}');
      locationController.text =
          '${_position?.longitude.toString().substring(1, 7)}, ${_position.latitude.toString().substring(1, 7)}';
    }

    // form fields
    final name = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      validator: this._isRequired,
      controller: customerNameController,
      decoration: InputDecoration(
        labelText: 'Customer Name',
        hintText: 'Customer Name',
      ),
    );

    final phone = TextFormField(
      controller: phoneController,
      keyboardType: TextInputType.phone,
      autofocus: false,
      validator: this._isRequired,
      decoration:
          InputDecoration(hintText: 'Phone Number', labelText: 'Phone Number'),
    );

    final dob = InkWell(
      splashColor: Colors.lightBlueAccent,
      onTap: () {
        _selectDate(context);
      },
      child: TextFormField(
        controller: dobController,
        validator: _isRequired,
        enabled: false,
        decoration: InputDecoration(
            hintText: _date.toString().split(' ')[0],
            labelText: 'Date of Birth'),
      ),
    );

    final location = TextFormField(
      controller: locationController,
      keyboardType: TextInputType.text,
      autofocus: false,
      enabled: false,
      validator: this._isRequired,
      decoration: InputDecoration(
        hintText: 'Location',
        labelText: 'Location',
      ),
    );

    final saveBtn = RaisedButton(
      child: Text('Save Customer'),
      textColor: Colors.white,
      color: Color.fromRGBO(58, 66, 86, 1.0),
      // disable button if location controller is empty or form is busy submitting
      onPressed: (locationController.text.isNotEmpty || _busy)
          ? () {
              this.submit();
              setState(() {
                _busy = true;
              });
              scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text('Saving data...'),
              ));
            }
          : null,
    );

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('New Customer'),
        elevation: 0.1,
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      ),
      body: Container(
        child: Form(
          key: this._formKey,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              SizedBox(height: 14.0),
              name,
              SizedBox(height: 4.0),
              phone,
              SizedBox(height: 4.0),
              dob,
              SizedBox(height: 4.0),
              location,
              SizedBox(height: 24.0),
              saveBtn,
            ],
          ),
        ),
      ),
    );
  }
}
