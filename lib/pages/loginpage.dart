import 'package:flutter/material.dart';
import '../services/auth.dart';

// Login using Google Sign In
class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignedIn});

  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  loginWithGoogle() async {
    widget.auth.signInWithGoogle().then((res) => widget.onSignedIn());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Center(
        child: MaterialButton(
          color: Color.fromRGBO(58, 66, 86, 1.0),
          elevation: 8.0,
          child: const Text('Login with Google'),
          textColor: Colors.white,
          onPressed: () {
            this.scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Row(
                    children: <Widget>[
                      CircularProgressIndicator(),
                      SizedBox(
                        width: 18.0,
                      ),
                      Text('Logging in...')
                    ],
                  ),
                  duration: Duration(seconds: 60),
                ));

            loginWithGoogle();
          },
        ),
      ),
    );
  }
}
