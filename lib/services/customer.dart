import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

// specifically for saving to PEG endpoint
class CustomerService {
  String url = 'http://174.138.6.99:3002/case_study_request';

  Future<String> saveToPeg(body) async {
    var response = await http.post(Uri.encodeFull(url),
        body: json.encode(body),
        headers: {"Accept": "application/json"});

    return response.body;
  }
}
