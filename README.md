# ForPEG

A Flutter project as case study for a job interview at PEG

## Getting Started

This project is a starting point for a Flutter application. This application, although with a single code-base, can be compiled to both Android and iOS.

The steps below should help compile this project to Android's apk.

To get this project running:

 - Setup up your Flutter environment: [Install & Setup Flutter](https://flutter.io/docs/get-started/install)
 - Git clone this project
 - Run `flutter doctor` to verify all is well
 - Connect your Android phone (with ADB DebugMode enabled) to your computer
 - Run `flutter build apk --build-name=<any integer> --build-version=1.x.x`

To run an already built release APK of this project, download from here: [ForPEG APK](https://drive.google.com/open?id=1xL3ZZ2Z05uuzbyxuRUpa8w7REwF-LqFO)

_Note: I do not have an iPhone, thus have not been able to test the iOS build_

## PWA

There's a counterpart Progressive Web App version to this mobile app, accessible at [peg.khophi.tech](https://peg.khophi.tech).